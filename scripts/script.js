let eyeIcon = document.getElementById('eyeicon')
let password = document.getElementById('password')

eyeIcon.addEventListener('click', () => {
    if (password.type == "password") {
        password.type = "text"
        eyeIcon.src = "./images/eye-solid.svg"
    } else {
        password.type = "password"
        eyeIcon.src = "./images/eye-slash-solid.svg"
    }
})